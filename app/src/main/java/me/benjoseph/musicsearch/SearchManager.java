package me.benjoseph.musicsearch;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.List;

import me.benjoseph.musicsearch.util.ResponseParser;
import me.benjoseph.musicsearch.util.SearchRequest;

/**
 * Created by josephben on 21/01/18.
 */

public class SearchManager {

    private static final String TAG = SearchManager.class.getSimpleName();

    private RecyclerView mSearchResultView;
    private String mSearchType;
    private String mSearchTerm;
    private SearchResultAdapter mResultAdapter;

    public SearchManager(String searchType, String searchTerm, RecyclerView resultView) {
        mSearchType = searchType;
        mSearchTerm = searchTerm;
        mSearchResultView = resultView;
        View.OnClickListener loadmoreListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Current item count: " + mResultAdapter.getItemCount());
                search(mResultAdapter.getItemCount() + 10 - 1/*to account for load more*/);
            }
        };
        mResultAdapter = new SearchResultAdapter(loadmoreListener);
        mSearchResultView.setAdapter(mResultAdapter);
    }

    public void search(int howMany) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.executeSearch(mSearchType, mSearchTerm, howMany, handler);
    }

    private final Handler handler = new Handler() {

        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            String data = bundle.getString("message", "");
            ResponseParser parser = new ResponseParser();
            List<SearchResultItem> list = parser.getSearchResultItemListFromJsonString(data);
            mResultAdapter.setData(list);
            mResultAdapter.notifyDataSetChanged();
        }
    };


}
