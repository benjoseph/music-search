package me.benjoseph.musicsearch.util;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by josephben on 20/01/18.
 */

public class SearchRequest {

    private static final String TAG = SearchRequest.class.getSimpleName();

    public void executeSearch(String searchType, String searchTerm, int howMany, final Handler handler) {
        String urlEncodedSearchTerm = "";
        try {
            urlEncodedSearchTerm = URLEncoder.encode(searchTerm, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final String urlString = String.format("https://itunes.apple.com/search?term=%s&limit=%d"/*&entity=%s"*/, urlEncodedSearchTerm, howMany/*,searchType*/);
        Log.d(TAG, "GET: " + urlString);

        Thread background = new Thread(new Runnable() {
            URL url = null;

            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                HttpURLConnection httpURLConnection = null;
                InputStream inputStream = null;
                try {
                    url = new URL(urlString);
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    inputStream = httpURLConnection.getInputStream();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int length = 0;
                    while ((length = inputStream.read(buffer)) != -1) {
                        baos.write(buffer, 0, length);
                    }
                    String response = baos.toString();
                    Log.d(TAG, "Response: " + response);
                    sendMessage(response);

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            private void sendMessage(String msg) {
                if (msg != null && !msg.equals("")) {
                    Message msgObj = handler.obtainMessage();
                    Bundle b = new Bundle();
                    b.putString("message", msg);
                    msgObj.setData(b);
                    handler.sendMessage(msgObj);
                }
            }
        });
        background.start();
    }
}
