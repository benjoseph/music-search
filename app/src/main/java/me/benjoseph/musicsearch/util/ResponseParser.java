package me.benjoseph.musicsearch.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.benjoseph.musicsearch.SearchResultItem;

/**
 * Created by josephben on 20/01/18.
 */

public class ResponseParser {

    private static final String RESULT_COUNT = "resultCount";
    private static final String RESULTS = "results";
    private static final String IMAGE_URL = "artworkUrl100";
    private static final String TYPE = "kind";
    private static final String ARTIST = "artistName";
    private static final String COLLECTION = "collectionName";

    public List<SearchResultItem> getSearchResultItemListFromJsonString(String data) {
        List<SearchResultItem> itemList = new ArrayList<>();
        try {
            JSONObject resultJsonObject = new JSONObject(data);
            int count = resultJsonObject.getInt(RESULT_COUNT);
            JSONArray resultArray = resultJsonObject.getJSONArray(RESULTS);
            for (int i = 0; i < count; i++) {
                JSONObject item = resultArray.getJSONObject(i);
                String imageUrl = "";
                String type = "";
                String artist = "";
                String collection = "";
                if (item.has(IMAGE_URL)) {
                    imageUrl = item.getString(IMAGE_URL);
                }
                if (item.has(TYPE)) {
                    type = item.getString(TYPE);
                }
                if (item.has(ARTIST)) {
                    artist = item.getString(ARTIST);
                }
                if (item.has(COLLECTION)) {
                    collection = item.getString(COLLECTION);
                }
                itemList.add(new SearchResultItem(imageUrl, type, artist, collection));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return itemList;
    }

}
