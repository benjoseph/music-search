package me.benjoseph.musicsearch.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by josephben on 21/01/18.
 */

public class ImageLazyLoader {

    private Map<String, Bitmap> cache = new HashMap<String, Bitmap>();
    ExecutorService executorService;
    private Handler mHandler;

    public ImageLazyLoader() {
        mHandler = new Handler(Looper.getMainLooper());
        executorService = Executors.newFixedThreadPool(10);
    }

    public void setImageFromUrlToView(String url, ImageView view) {
        if (cache.containsKey(url)) {
            view.setImageBitmap(cache.get(url));
            return;
        }
        DownloadImageTask downloadImageTask = new DownloadImageTask(url, view);
        executorService.submit(downloadImageTask);
    }

    private void setBitmapToViewFromUIThread(final Bitmap bitmap, final ImageView view) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                view.setImageBitmap(bitmap);
            }
        });
    }

    class DownloadImageTask implements Runnable {
        private String mUrl;
        private ImageView mImageView;

        public DownloadImageTask(String url, ImageView view) {
            mUrl = url;
            mImageView = view;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            try {
                URL imageUrl = new URL(mUrl);
                HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
                InputStream is = conn.getInputStream();
                Bitmap image = BitmapFactory.decodeStream(is);
                cache.put(mUrl, image);
                setBitmapToViewFromUIThread(image, mImageView);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
