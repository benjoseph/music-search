package me.benjoseph.musicsearch.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import me.benjoseph.musicsearch.R;
import me.benjoseph.musicsearch.SearchManager;
import me.benjoseph.musicsearch.SearchResultAdapter;
import me.benjoseph.musicsearch.SearchResultItem;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();

    String searchType = "all";
    List<SearchResultItem> list;
    SearchResultAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //spinner for search type
        Spinner searchTypeSpinner = (Spinner) findViewById(R.id.search_type);
        ArrayAdapter<CharSequence> searchTypeAdapter = ArrayAdapter.createFromResource(this,
                R.array.searchType, android.R.layout.simple_spinner_item);
        searchTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        searchTypeSpinner.setAdapter(searchTypeAdapter);

        searchTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                searchType = (String) parent.getItemAtPosition(position);
                // Toast.makeText(getApplicationContext(),searchType,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.search_results_rv);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator()); //do i need this?
        recyclerView.addItemDecoration(new DividerItemDecoration(MainActivity.this, LinearLayoutManager.VERTICAL));

        //for search input
        final EditText searchInputView = (EditText) findViewById(R.id.search_input);

        //initiate search
        Button searchButton = (Button) findViewById(R.id.search_button);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchString = searchInputView.getText().toString();
                Log.d(TAG, "Calling search with searchType: " + searchType + " and searchString: " + searchString);
                search(searchType, searchString, recyclerView);
            }
        });


    }

    private void search(String searchType, String searchString, RecyclerView resultView) {
        SearchManager manager = new SearchManager(searchType, searchString, resultView);
        manager.search(10);
    }

}
