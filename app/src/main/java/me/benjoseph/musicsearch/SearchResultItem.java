package me.benjoseph.musicsearch;

/**
 * Created by josephben on 20/01/18.
 */

public class SearchResultItem {

    private String mImageUrl;
    private String mType;
    private String mArtist;
    private String mCollection;

    public SearchResultItem(String imageURl,
                            String type,
                            String artist,
                            String collection) {
        mImageUrl = imageURl;
        mType = type;
        mArtist = artist;
        mCollection = collection;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }


    public String getArtist() {
        return mArtist;
    }

    public void setArtist(String artist) {
        this.mArtist = artist;
    }


    public String getCollection() {
        return mCollection;
    }

    public void setCollection(String collection) {
        this.mCollection = collection;
    }


    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.mImageUrl = imageUrl;
    }
}
