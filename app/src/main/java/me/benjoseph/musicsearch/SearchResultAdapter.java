package me.benjoseph.musicsearch;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import me.benjoseph.musicsearch.util.ImageLazyLoader;

/**
 * Created by josephben on 20/01/18.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.SearchResultViewHolder> {

    private static final String TAG = SearchResultAdapter.class.getSimpleName();
    private View.OnClickListener mLoadmoreListener;
    private ImageLazyLoader imageLazyLoader;
    private boolean isFetching = false;

    private List<SearchResultItem> mSearchResultItemsList = new ArrayList<>();

    public SearchResultAdapter(View.OnClickListener loadmoreListener) {
        mLoadmoreListener = loadmoreListener;
        imageLazyLoader = new ImageLazyLoader();
    }

    public void setData(List<SearchResultItem> itemList) {
        mSearchResultItemsList = itemList;
        SearchResultItem footer = new SearchResultItem("", "test", "", "");
        mSearchResultItemsList.add(footer);//for load more
        isFetching = false;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mSearchResultItemsList.size() - 1) {
            return SearchResultViewHolder.TYPE_FOOTER;
        } else {
            return SearchResultViewHolder.TYPE_NORMAL;
        }
    }

    @Override
    public SearchResultAdapter.SearchResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == SearchResultViewHolder.TYPE_NORMAL) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_result_item, parent, false);
            return new SearchResultViewHolder(itemView);
        } else if (viewType == SearchResultViewHolder.TYPE_FOOTER) {
            View footer = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_result_load_more, parent, false);
            return new SearchResultViewHolder(footer);
        }
        return null;//add logs
    }

    @Override
    public void onBindViewHolder(final SearchResultAdapter.SearchResultViewHolder holder, int position) {
        if (getItemViewType(position) == SearchResultViewHolder.TYPE_NORMAL) {
            SearchResultItem resultItem = mSearchResultItemsList.get(position);
            holder.mImageView.setImageResource(R.drawable.img);
            imageLazyLoader.setImageFromUrlToView(resultItem.getImageUrl(), holder.mImageView);
            holder.mTypeTv.setText(resultItem.getType());
            holder.mArtistTv.setText(resultItem.getArtist());
            holder.mCollectionTv.setText(resultItem.getCollection());
        } else {
            holder.mLoadmoreTv.setText("Click to load more");
//            holder.itemView.setOnClickListener(mLoadmoreListener);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLoadmoreListener.onClick(v);
                    holder.mLoadmoreTv.setVisibility(View.GONE);
                    holder.mProgressBar.setVisibility(View.VISIBLE);
                    isFetching = true;
                }
            });

            if (isFetching) {
                holder.mProgressBar.setVisibility(View.VISIBLE);
                holder.mLoadmoreTv.setVisibility(View.GONE);
            } else {
                holder.mProgressBar.setVisibility(View.GONE);
                holder.mLoadmoreTv.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mSearchResultItemsList.size();
    }

    public class SearchResultViewHolder extends RecyclerView.ViewHolder {

        public static final int TYPE_NORMAL = 0;
        public static final int TYPE_FOOTER = 1;

        private ImageView mImageView;
        private TextView mTypeTv;
        private TextView mArtistTv;
        private TextView mCollectionTv;
        private TextView mLoadmoreTv;
        private ProgressBar mProgressBar;

        public SearchResultViewHolder(View view) {
            super(view);
            if (view.getId() == R.id.search_results_item_layout) {
                mImageView = (ImageView) view.findViewById(R.id.result_image);
                mTypeTv = (TextView) view.findViewById(R.id.result_type);
                mArtistTv = (TextView) view.findViewById(R.id.result_artist);
                mCollectionTv = (TextView) view.findViewById(R.id.result_collection);
            } else {
                mLoadmoreTv = (TextView) view.findViewById(R.id.loadmore_tv);
                mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            }
        }
    }
}
